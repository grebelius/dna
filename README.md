# Konvertera BAM till FASTA

http://bedtools.readthedocs.io/en/latest/content/tools/bamtofastq.html

bedtools bamtofastq [OPTIONS] -i <BAM> -fq <FASTQ>

